/*
 * $ g++ PalabrasHebras.cpp -o PalabrasHebras -lpthread
 * para ver hebras:
 * ps -eLf
 * pstree
 * top -H -u usuario (Shift + f para cambiar vista)
 */

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <fcntl.h>
#include <string.h>
#include <sstream>
#include <fstream>
#include <chrono>
#include <thread>
using namespace std;

int lectorlineas(string texto) {
    ifstream file(texto);
    string str;
    int lineas = 0;
    while (getline(file, str)) {
        //cout << str << endl;
        lineas++;
        for (int i = 0; i < str.length(); i++) {
            //cout << str[i]; //Comentado para que no imprima el texto, si lo quiere imprimir, borrar el comentado en esta linea como en la linea 27
        }
        //cout << endl;
    }

    //cout << "El texto " << "contiene " << lineas << " lineas." << endl;
    //Este contador cuenta las lineas que "separan un p�rrafo" ejemplo
    //Oigan la cena est� servida, tienen hambre?
    //
    //Claro (3 lineas, lo de arriba cuenta)
    sleep(1);  //18 segundos aprox con todos los sleep activados, 0.628 aprox sin los sleep
    return lineas;
}
int lectorpalabras(string texto) {
    ifstream file(texto);
    int palabras = 0;
    string palabra;
    while (file >> palabra) {
        palabras++;

    }
    //cout << "El texto contiene " << palabras << " palabras." << endl;
    sleep(1);
    return palabras;
}

int lectorcaracteres(string texto) {
    ifstream file(texto);
    int caracteres = 0;
    char carac;
    while (file >> carac) {
        caracteres++;
    }
    //cout << "El texto contiene " << caracteres << " caracteres, sin contar los espacios." << endl;
    sleep(1);
    return caracteres;

}


int main(int argc, char* argv[]) {
    auto start = chrono::system_clock::now();
    int contadormainlineas = 0;
    int contadormainpalabras = 0;
    int contadormaincaracteres = 0;
    for (int i = 1; i < argc; i++) {
        cout << "-------Cual texto sera analizado? R: " << argv[i] << "-------" << endl;
        //creaci�n de las hebras y join, si escribo los join uno detras del otro, se entremezclan los outputs.
        thread hebralinea(lectorlineas, argv[i]);
        cout << "El texto " << "contiene " << lectorlineas(argv[i]) << " lineas." << endl;
        hebralinea.join();
        thread hebrapalabra(lectorpalabras, argv[i]);
        cout << "El texto contiene " << lectorpalabras(argv[i]) << " palabras." << endl;
        hebrapalabra.join();
        thread hebracaracter(lectorcaracteres, argv[i]);
        cout << "El texto contiene " << lectorcaracteres(argv[i]) << " caracteres, sin contar los espacios." << endl;
        hebracaracter.join();
        //contador para saber el total de cada apartado
        contadormainlineas = contadormainlineas + lectorlineas(argv[i]);
        contadormainpalabras = contadormainpalabras + lectorpalabras(argv[i]);
        contadormaincaracteres = contadormaincaracteres + lectorcaracteres(argv[i]);
    }

    cout << "------- TOTAL -------" << endl;
    cout << "Cantidad de lineas totales: " << contadormainlineas << "." << endl;
    cout << "Cantidad de palabras totales: " << contadormainpalabras << "." << endl;
    cout << "Cantidad de caracteres totales: " << contadormaincaracteres << "." << endl;
    //temporizador visto en clases
    auto end = chrono::system_clock::now();
    chrono::duration<float> secs = end - start;
    cout << secs.count() << " segundos se demor� el programa en ejecutarse." << endl;
    return 0;
}