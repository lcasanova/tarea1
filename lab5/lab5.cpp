#include<bits/stdc++.h>
using namespace std;

int Costo[100][100], n;

int getMin(int distancia[], bool visitado[]) {
    int clave = 0;
    int min = INT_MAX;
    for (int i = 0; i < n; i++) {
        if (!visitado[i] && distancia[i] < min) {
            min = distancia[i];
            clave = i;
        }
    }
    return clave;
}

void imprime(int distancia[], int par[]) {
    for (int i = 0; i < n; i++) {
        int temp = par[i];
        cout << i << " <- ";
        while (temp != -1)
        {
            cout << temp << " <- ";
            temp = par[temp];
        }
        cout << endl;
        cout << "::::Distancia = " << distancia[i];
        cout << endl;
    }
}


void dijkstra(int fuente) {
    int par[100], distancia[100];
    bool visitado[100] = { 0 };
    fill(distancia, distancia + n, INT_MAX);

    distancia[fuente] = 0;
    par[fuente] = -1;

    for (int g = 0; g < n - 1; g++) {
        int u = getMin(distancia, visitado);
        visitado[u] = true;
        cout << " min = " << u << endl;
        for (int v = 0; v < n; v++) {
            if (!visitado[v] && (distancia[u] + Costo[u][v]) < distancia[v] && Costo[u][v] != 9999)
            {
                par[v] = u;
                distancia[v] = distancia[u] + Costo[u][v];
            }
        }
    }

    imprime(distancia, par);
}



int main(void) {
    cout << "Ingresar n : ";
    cin >> n;
    cout << "Ingresar dinamica de red espaciotemporal : \n";
    //esto es lo que gu�a las decisiones sobre el destino de las c�lulas
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++)cin >> Costo[i][j];
    }
    int src;
    cout << "\nIngresar fuente : ";  cin >> src;
    dijkstra(src);
}