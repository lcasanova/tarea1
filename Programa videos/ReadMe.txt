Todo eso de dbz se explica mejor por el link que usé cuando estaba escribiendo el código, https://www.youtube.com/watch?v=xQ2JP8dJ_k0;
que el nombre de los archivos descargados/transformados sea barney.x es por otro video de los que estaba usando, si
ha visto himym le hará sentido, si no la ha visto, debería, https://www.youtube.com/watch?v=aIEZFaGzaH8.


RESUMEN
PARA EJECUTAR ESTE PROGRAMA SE REQUIERE DE youtube-dl y de ffmpeg, el primero para descargar, el segundo para convertir a un audio
y para reproducir posteriormente con ffplay (también se puede usar vlc o mplayer).
IMPORTANTE el programa debe ejecutarlo ./x y una vez ejecutado, le pedirá la url del video que desee (máx 100 caracteres).
El programa guarda todas las variables como barney.mp3 y barney.mp4 que serán los 2 archivos que obtendrá al ejecutar el programa,
el video que descargó entregando la URL (cualquiera) y la extracción de audio para dicho video.
Programa probado con url otorgada dentro del código y con diferentes canciones/videos probados, con la versión de 
pedirsela al usuario.

Creé 2 procesos hijos, uno que descarga y el otro que convierte y luego el proceso padre ejecuta.

