/*
 * $ g++ Otros.cpp -o Otros
 */
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string>
using namespace std; 
class Fork {
private:
    pid_t pid;
    int segundos;


public:
    // Constructor
    Fork(int seg) {
        segundos = seg;
        ejecutarCodigo();
    }

    // M�todos.

    void ejecutarCodigo() {
        pid_t pid1 = fork();
        if (pid1 > 0) {
            wait(NULL);

            pid_t pid2 = fork();
            if (pid2 > 0) {

                // C�digo proceso padre.
                // Padre espera por el t�rmino del proceso hijo.
                wait(NULL);
                wait(0);
                wait(NULL);
                sleep(segundos);
                cout << "Bien hecho hijo " << endl;
                cout << "Yo me encargo desde aqu�... " << getpid() << endl;
                execlp("ffplay", "ffplay", "barney.mp3", NULL);
                cout << endl;
            }
            else if (pid2 == 0) {
                cout << "ahora da el golpe final(extrae el audio)";

                execlp("ffmpeg", "ffmpeg", "-i", "barney.mp4", "-acodec", "libmp3lame", "-ac", "2", "-ab", "128k", "barney.mp3", (char*)NULL);
                cout << "listo";
                cout << endl;
                sleep(segundos);
                wait(NULL);
               
            }
        }


        else if (pid1 == 0) {
            // C�digo del proceso hijo 1.
            //Justo estaban dando un marat�n de pel�culas de dbz profe
            cout << "Gohan, demu�strale tus verdaderos poderes a esos sujetos ";
            //descargar el video le cuesta
            cout << endl;
            cout << "Pppap�... " << getpid();
            cout << endl;
            cout << "Tu deber es proteger a la Tierra ";
            cout << endl;
            cout << "Mi pap�...mi pap� me ayud� ";
            //Agarra fuerzas para volver a pelear (descargar el video)
            cout << endl;
            //youtube dl (prueba con una url desde el c�digo)
            //////////execlp("youtube-dl", "youtube-dl", "https://www.youtube.com/watch?v=xQ2JP8dJ_k0", "-o", "dbz.mp4", (char*)NULL);
            char url2[100];
            cout << "Ingrese el enemigo a abatir: ";
            cin.getline(url2, 100);
            //cout << url2; (para comprobar que se guardaba bien la url pedida al usuario)
            cout << endl;
            execlp("youtube-dl", "youtube-dl", "-f", "mp4", "-o", "barney.mp4", url2, (char*)NULL);

            // Espera "segundos" para continuar (para pruebas).
            sleep(segundos);

        }
    }
};
int main() {
    // Instanciaci�n.
    Fork miFork(1);

    return 0;
}