#include<iostream>
#include<string>

using namespace std;

class Persona{
	int rut;
	string nombre;

	public:
		void setPersona(){
			cout<<"Ingrese RUT: ";
			cin >> rut;
			
			cout<<"Ingrese Nombre: ";
			cin >> nombre;
		}

		void mostrarPersona(){
			cout << endl << rut <<"\t" << nombre;
		}
};

class Estudiante: public Persona{
	string curso;
	int costoCurso;
	
	public:
	    void setEstudiante(){
			setPersona();

			cout << "Ingresar Curso:";
			cin >> curso;

			cout << "Ingresar Costo de Curso:";
			cin >> costoCurso;
		}
		
		void mostrarEstudiante(){
			mostrarPersona();
	
    		cout << "\t" << curso << "\t" << costoCurso << endl;
		}
};

int main(){
	Estudiante s1, s2;

    s1.setEstudiante();
	s1.mostrarEstudiante();

    s2.setEstudiante();
    s2.mostrarEstudiante();
    s2.mostrarPersona();

	return 0;
}