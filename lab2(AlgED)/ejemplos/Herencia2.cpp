#include<iostream>
#include<string>

using namespace std;

class Persona{
    protected:
        string rut;
        string nombre;

	public:
        Persona(string r, string n){
            rut = r;
            nombre = n;
        }

        string getRut(){
            return rut;
        }

        string getNonbre(){
            return nombre;
        }

        void setRut(string r){
            rut = r;
        }

        void setNombre(string n){
            nombre = n;
        }
};

class Deportista{
    protected:
        int peso;
        string deporte;
    
    public:
        Deportista(int p, string d){
            peso = p; deporte = d;
        }

        string getDeporte(){
            return deporte;
        }
};

class Estudiante: public Persona, public Deportista{
    protected:
        string curso;
        int costoCurso;
        
	public:
        Estudiante(string c, int cc, string r, string n, int p, string d): Persona(r, n), Deportista(p, d){
            curso= c;
            costoCurso=cc;
        }    

        string getCurso(){
            return curso;
        }
        
        int getCostoCurso(){
            return costoCurso;
        }
};

int main(){
	Estudiante s1("2do Básico", 4, "12.345.567-K", "Diego Núñez", 100, "Atletismo");

    cout << s1.getNonbre() << ", " << s1.getDeporte() << endl;

   
	return 0;
}