#include<iostream>
#include<string>
#include<stdio.h>
using namespace std;


class Stack {
public:
	Stack(int size = 10);			// constructor
	~Stack() { delete[] values; }	// destructor
	bool IsEmpty() { return top == -1; }
	bool IsFull() { return top == maxTop; }
	int Top();   // examine, without popping
	void Push(int x);
	int Pop();
	void DisplayStack();
private:
	int maxTop;		// max stack size = size - 1
	int top;		// current top of stack
	int* values=0;	// element array
};
class StackO {
public:
	StackO(int size = 10);			// constructor
	~StackO() { delete[] valuesO; }	// destructor
	bool IsEmptyO() { return topO == -1; }
	bool IsFullO() { return topO == maxTopO; }
	string TopO();   // examine, without popping
	void PushO(char O);
	string PopO();
	void DisplayStackO();
private:
	int maxTopO;		// max stack size = size - 1
	int topO;		// current top of stack
	string* valuesO;	// element array
};

Stack::Stack(int size /*= 10*/) {
	values = new int[size];
	top = -1;
	maxTop = size - 1;
}
StackO::StackO(int size /*= 10*/) {
	valuesO = new string[size];
	topO = -1;
	maxTopO = size - 1;
}

void Stack::Push(int x) {
	if (IsFull())  // if stack is full, print error
		cout << "Error: the stack is full." << endl;
	else
		values[++top] = x;
}
void StackO::PushO(char O) {
	if (IsFullO())  // if stack is full, print error
		cout << "Error: the stack is full." << endl;
	else
		valuesO[++topO] = O;
}
int Stack::Pop() {
	if (IsEmpty()) { //if stack is empty, print error
		cout << "Error: the stack is empty." << endl;
		return -1;
	}
	else {
		return values[top--];
	}
}
string StackO::PopO() {
	if (IsEmptyO()) { //if stack is empty, print error
		cout << "Error: the stack is empty." << endl;
		return "";
	}
	else {
		return valuesO[topO--];
	}
}
int Stack::Top() {
	if (IsEmpty()) {
		cout << "Error: the stack is empty." << endl;
		return -1;
	}
	else
		return values[top];
}
string StackO::TopO() {
	if (IsEmptyO()) {
		cout << "Error: the stack is empty." << endl;
		return "";
	}
	else
		return valuesO[topO];
}
void Stack::DisplayStack() {
	cout << "top -->";
	for (int i = top; i >= 0; i--)
		cout << "\t|\t" << values[i]-48 << "\t|" << endl;
	cout << "\t|---------------|" << endl;
}
void StackO::DisplayStackO() {
	cout << "top -->";
	for (int i = topO; i >= 0; i--)
		cout << "\t|\t" << valuesO[i] << "\t|" << endl;
	cout << "\t|---------------|" << endl;
}
	
int main(void) {
	int largo = 0;
	int valido = 1;
	cout << "INDICAR EL LARGO DE LA EXPRESION!!: (contando n�meros del 1 la 9 y operadores)\n";
	cin >> largo;
	Stack stack(largo);
	StackO stackO(largo);
	char ejemplo[100];
	cout << "Ingrese expresi�n matem�tica: \n";
	scanf("%s", ejemplo);
	for (int i = 0; i < largo; i++) {
		if (ejemplo[i] == '+' || ejemplo[i] == '-' || ejemplo[i] == '*' || ejemplo[i] == '/') {
			stackO.PushO(ejemplo[i]);

		}
		else {
			stack.Push(ejemplo[i]);
		}
			//cout << ejemplo[i]<<endl;
	}
	stack.DisplayStack();
	stackO.DisplayStackO();
	int resultado = 0;
	string op = stackO.TopO();
	cout << op << endl;
	if (op == "+") {
		int x = stack.Top() - 48;
		stack.Pop();
		int y = stack.Top() - 48;
		stack.Pop();
		stackO.PopO();
		resultado = x + y;
		cout << x << "+" << y << "=" << endl;
		cout << resultado<<endl;
	}
	else if (op == "-") {
		int x = stack.Top() - 48;
		stack.Pop();
		int y = stack.Top() - 48;
		stack.Pop();
		stackO.PopO();
		resultado = x - y;
		cout << x << "-" << y << "=" << endl;
		cout << resultado<<endl;
	}
	else if (op == "*") {
		int x = stack.Top() - 48;
		stack.Pop();
		int y = stack.Top() - 48;
		stack.Pop();
		stackO.PopO();
		resultado = x * y;
		cout << x << "*" << y << "=" << endl;
		cout << resultado<<endl;
	}
	else if (op == "/") {
		int x = stack.Top() - 48;
		stack.Pop();
		int y = stack.Top() - 48;
		stack.Pop();
		stackO.PopO();
		resultado = x / y;
		cout << x << "/" << y << "=" << endl;
		cout << resultado<<endl;
	}
	else {}
	while (stack.IsEmpty() == 0) {
		string op = stackO.TopO();
		if (op == "+") {
			int x = resultado;
			int y = stack.Top() - 48;
			stack.Pop();
			stackO.PopO();
			resultado = x + y;
			cout << x << "+" << y << "=" << endl;
			cout << resultado << endl;
		}
		else if (op == "-") {
			int x = resultado;
			int y = stack.Top() - 48;
			stack.Pop();
			stackO.PopO();
			resultado = x - y;
			cout << x << "-" << y << "=" << endl;
			cout << resultado << endl;
		}
		else if (op == "*") {
			int x = resultado;
			int y = stack.Top() - 48;
			stack.Pop();
			stackO.PopO();
			resultado = x * y;
			cout << x << "*" << y << "=" << endl;
			cout << resultado << endl;
		}
		else if (op == "/") {
			int x = resultado;
			int y = stack.Top() - 48;
			stack.Pop();
			stackO.PopO();
			resultado = x / y;
			cout << x << "/" << y << "=" << endl;
			cout << resultado << endl;
		}
		else {}

	}
	return 0;
}


