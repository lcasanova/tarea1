#include "grafo.h"


GrafoTipo::GrafoTipo(int maxV){
   numVertices = 0;
   maxVertices = maxV;
   vertices = new int[maxV];
   aristas = new int*[maxV];
 
   for(int i = 0; i < maxV; i++)
     aristas[i] = new int[maxV];
 
   marca = new bool[maxV];
 

}
void DepthFirstSearch(GrafoTipo<VerticeTipo> grafo, VerticeTipo inicioVertice, VerticeTipo finalVertice)
{
    StackTipo<VerticeTipo> stack;
    QueType<VerticeTipo> verticeQ;

    bool found = false;
    VerticeTipo vertex;
    VerticeTipo item;

    grafo.LimpiarMarcas();
    stack.Push(inicioVertice);
    do {
        stack.Pop(vertice);
        if (vertice == finalVertice)
            found = true;
    }
}


GrafoTipo::~GrafoTipo(){
   delete [] vertices;
 
   for(int i = 0; i < maxVertices; i++)
      delete [] aristas[i];
 
   delete [] aristas;
   delete [] marca;
} 

void GrafoTipo::AnadirVertice(int vertice){
   vertices[numVertices] = vertice;

   for(int index = 0; index < numVertices; index++) {
     aristas[numVertices][index] = ARISTA_NULO;
     aristas[index][numVertices] = ARISTA_NULO;
   }
   
   numVertices++;
}

void GrafoTipo::AnadirArista(int desdeVertice, int aVertice, int Valor){
   int fila, columna;

   fila = IndiceEs(vertices, desdeVertice);
   columna = IndiceEs(vertices, aVertice);
   aristas[fila][columna] = Valor;
} 

int GrafoTipo::ValorEs(int desdeVertice, int aVertice){
   int fila, columna;

   fila = IndiceEs(vertices, desdeVertice);
   columna = IndiceEs(vertices, aVertice);
   
   return aristas[fila][columna];
} 

int GrafoTipo::IndiceEs(int *vertices, int vertice){
  for(int i = 0; i < numVertices; i++){
      if (vertices[i] == vertice)
          return i;
  }

  return -1;
}

bool GrafoTipo::EstaLleno(){
  if (numVertices == maxVertices)
      return true;
  
  return false;
}

bool GrafoTipo::EstaVacio(){
  if (numVertices == 0)
      return true;
  
  return false;
}

int GrafoTipo::TamanoGrafo() {
    return numVertices;
}