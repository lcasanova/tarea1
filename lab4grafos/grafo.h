const int ARISTA_NULO = 0;
class GrafoTipo {
  public:
    GrafoTipo(int);
    ~GrafoTipo();
    void AnadirVertice(int);
    void AnadirArista(int, int, int);
    int ValorEs(int, int);
    int IndiceEs(int*, int);
    bool EstaVacio();
    bool EstaLleno(); 
    int TamanoGrafo();

 private:
    int numVertices;
    int maxVertices;

    int* vertices;
    int **aristas;

    bool* marca;
}; 
