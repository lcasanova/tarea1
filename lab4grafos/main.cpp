#include <iostream> 
#include "grafo.h" 

using namespace std; 

int main() { 
    GrafoTipo x(10); 

    cout << "Vació? " << x.EstaVacio() << " - " << x.TamanoGrafo() << endl;

    
    x.AnadirVertice(10);
    x.AnadirVertice(20);
    cout << "\nVació? " << x.EstaVacio() << " - " << x.TamanoGrafo() << endl;
    x.AnadirArista(10, 20, 100);
    cout << "Peso: " << x.ValorEs(10, 20) << endl;
    x.AnadirVertice(30);
    cout << "\nVació? " << x.EstaVacio() << " - " << x.TamanoGrafo() << endl;
    x.AnadirArista(10, 30, 200);
    cout << "Peso: " << x.ValorEs(10, 30) << endl;
    
} 