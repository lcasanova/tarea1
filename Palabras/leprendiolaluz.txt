We have something for you.

WILL: My beautiful,
bossy Stella.

I guess it's true,
what that book of yours says.

That the soul knows no time.

My drug trial's not working.

I want you to know
that this past month

will last forever for me.

My only regret is that

you didn't get to see
your lights.

(CELL PHONE VIBRATING)

(ON SPEAKERS)
I finally got you speechless.

You know, people are always
saying if you love something,

you have to learn
to let it go.

I thought
that was such bullshit.

Till I watched you almost die.

In that moment, Stella,

nothing mattered to me.

Except you.

I'm sorry.

I don't wanna go.

All I want is to be with you.

I can't.

I need you to be safe.

From me.

I don't know what comes next,

but I don't regret
any of this.

Could you close your eyes?

I just don't know
if I can walk away

if you're still looking at me.

Please.

I love you

so much.

(MELLOW MUSIC PLAYING)

(SNIFFLES)
He left this for you.

(SOBBING)

(BOTH CHUCKLE)

(GASPS)

(SOBS)