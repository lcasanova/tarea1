PROGRAMA SIN HEBRAS
En esta misma carpeta que entregaré en git, van 3 textos, 1 es una escena de Casablanca y lo usé porque tenía el screenplay en el celular,
entonces es una buena cantidad de palabras, accesibles, copiar y pegar a un txt y poner en el programa que explicaré más adelante,
el otro texto es la letra de Come Here canción de Kath Bloom, hermosa canción que sale en la primera película (espectacular) de la trilogía Before,
y el otro texto es otra escena de una película que vi antes de terminar la tarea, Five Feet Apart, es una de las últimas escenas, así que si no
la ha visto, no lea el archivo (el programa tiene comentada la opción para imprimir el archivo en el output así que está a salvo por ese lado).

Ahora, con respecto al programa, tiene 3 funciones sin contar la main, una para cada requisito de conteo, lineas, palabras y caracteres, el programa
cuenta las lineas que están vacías que separan parráfos (ejemplo para entender mejor en la linea 30 del código del programa) y el conteo de caracteres es
sin contar los espacios, para verificar que todos estos conteos estaban en lo correcto, usé como referencia la función de Word que permite ver este tipo de información,
simplemente copié el contenido de los .txt y los pegué en una hoja en blanco, para sumar el total de datos de todos los textos introducidos, usé un contador 
en la función main, esos 3 textos están ahí para "facilitar" que se verifique el correcto funcionamiento de este programa, pero obviamente puede entregarle la cantidad de 
archivos que quiera y diferentes a los que contiene esta carpeta. Dentro de la función main también está el "temporizador", ejecutado con la libreria chrono. 