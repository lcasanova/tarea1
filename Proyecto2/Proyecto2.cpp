#include<iostream>
using namespace std;

int main() {

	string Superficie_Celular[] = {"Ligando hidrof�lico 1",
		"Ligando hidrof�lico 2",
		"Ligando hidrof�lico 3",
		"Receptor de canal i�nico 1",
		"Receptor de canal i�nico 2",
		"Receptor de canal i�nico 3",
		"GPCR 1",
		"GPCR 2",
		"GPCR 3",
		"FGFR",
		"EGFR",
		"HGFR",
		"NGFR"};
	int i = 0;
	//string mitogeno = "EGFR";
	string mitogeno;
	cout << "Introduzca el receptor que busca: ";
	cin >> mitogeno;
	string bandera = "f";

	while ((bandera == "f") && (i < 13)) {
		if (Superficie_Celular[i] == mitogeno) {
			bandera = "t";
		}
		i++;
	}
	if (bandera == "f") {
		cout << "El receptor adecuado para este mit�geno no se encuentra en la superficie de la c�lula." << endl;
	}
	else if (bandera == "t") {
		cout << "El receptor ha sido encontrado en la superficie de la c�lula y se encuentra a: " << i - 1 << " elementos." << endl;
	}
}