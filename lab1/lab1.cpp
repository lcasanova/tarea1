#include<iostream>
#include<string>

using namespace std;

class Estudiante {
private:
    string nombre;
    int edad;

public:
    Estudiante(string nombre, int edad) {
        this->nombre = nombre;
        this->edad = edad;
    }

    Estudiante() {
        this->nombre = "";
        this->edad = 0;
    }

    string getNombre() {
        return nombre;
    }

    void setNombre(string nombre) {
        this->nombre = nombre;
    }

    int getEdad() {
        return edad;
    }

    void setEdad(int edad) {
        this->edad = edad;
    }
};


//Por comodidad, es mejor definir la clase "Nodo" para luego definir la lista
//(cada nodo esta formado por los datos y por su "apuntador" al siguiente)
class NodoEstudiante {
private:
    Estudiante valor;
    NodoEstudiante* vecino;

public:
    NodoEstudiante() {
        this->vecino = NULL;
    }

    NodoEstudiante(Estudiante nuevo) {
        this->valor.setNombre(nuevo.getNombre());
        this->valor.setEdad(nuevo.getEdad());

        this->vecino = NULL;
    }

    void setVecino(NodoEstudiante* vec) {
        this->vecino = vec;
    }

    NodoEstudiante* getVecino() {
        return(this->vecino);
    }

    Estudiante* getEstudiante() {
        return(&this->valor);
    }
};  

class ListaEstudiantes {
private:
    NodoEstudiante* primero, * ultimo;

public:
    ListaEstudiantes() {
        primero = ultimo = NULL;
    }

    void insertarNodo(NodoEstudiante* nuevo) {
        if (ultimo == NULL) {
            primero = ultimo = nuevo;
        }
        else {
            ultimo->setVecino(nuevo);
            ultimo = nuevo;
        }
    }

    void mostrarLista() {
        NodoEstudiante* recorrer;

        recorrer = primero;
        cout << "---------------------\n";
        while (recorrer != NULL) {
            Estudiante* estudiante;
            estudiante = recorrer->getEstudiante();

            cout << "Estudiante" << endl;
            cout << "-- Nombre: " << estudiante->getNombre() << endl;;
            cout << "-- Edad: " << estudiante->getEdad() << endl;

            recorrer = recorrer->getVecino();
        }
    }
    void buscarEstudiante(string nnnombre) {
        NodoEstudiante* recorrer;

        recorrer = primero;
        int presente = 0;
        while (recorrer != NULL) {
            Estudiante* estudiante;
            estudiante = recorrer->getEstudiante();
            if (nnnombre == estudiante->getNombre())
            {
                //cout << "Est� presente este estudiante." << endl;
                presente = 1;
            }
            else
            {
                //cout << "No se encuentra presente este estudiante." << endl;
            }
            recorrer = recorrer->getVecino();
        }
        if (presente == 1) { cout << "Est� presente este estudiante." << endl; }
        else { cout << "No se encuentra presente este estudiante." << endl; }
    }
    void quitarPrimero() {
        NodoEstudiante* recorrer=primero;
        primero = primero->getVecino();
        recorrer->setVecino(nullptr);
        delete recorrer;
    }
    void quitarUltimo() {
        if (primero){
            NodoEstudiante* recorrer = primero;
            NodoEstudiante* antes = nullptr;
            while (recorrer->getVecino()) {
                antes = recorrer;
                recorrer = recorrer->getVecino();
            }
            if (recorrer == primero) {
                primero = primero->getVecino();
                delete recorrer;
            }
            else {
                antes->setVecino(nullptr);
                delete recorrer;
            }

        }
        else { cout << "No quedan estudiantes" << endl; }
    }
};
int main() {
    ListaEstudiantes lista;
    //Estudiante p("Estudiante 1", 20);
    int nalumnos = 0;
    cout << "Cu�ntos estudiantes hay?\n";
    cin >> nalumnos;
    string nnombre = "";
    string nnnombre = "";
    int eedad = 0;
    for (int i = 0; i < nalumnos; i++)
    {
        cout << "Ingrese edad\n";
        cin >> eedad;
        cout << "Ingrese nombre\n";
        getline(cin >> ws, nnombre);
        Estudiante p(nnombre, eedad);
        NodoEstudiante* nuevo = new NodoEstudiante(p);
        lista.insertarNodo(nuevo);
        }
    lista.mostrarLista();
    cout << "Qu� estudiante quiere buscar?\n";
    getline(cin >> ws, nnnombre);
    lista.buscarEstudiante(nnnombre);
    cout << "Se retira el primer estudiante: \n";
    lista.quitarPrimero();
    lista.mostrarLista();
    cout << "Se retira el ultimo estudiante: \n";
    lista.quitarUltimo();
    lista.mostrarLista();
    cout << "Qu� estudiante quiere buscar?\n";
    getline(cin >> ws, nnnombre);
    lista.buscarEstudiante(nnnombre);
return 0;
}
