#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>
#include<bits/stdc++.h>
using namespace std;

struct Vertice {
    Vertice(int v, std::set<int> a) : num_vertice(v), adyacente(a) {}
    int num_vertice;
    std::set<int> adyacente;
};

class Grafo {
public:
    Grafo(std::vector<Vertice> v) : v_(v) {}
    bool EsCaminable();
    std::vector<Vertice> v_;
};
bool Grafo::EsCaminable() {
    std::vector<int> grado(v_.size());
    for (auto v : v_) {
        for (auto e : v.adyacente) {
            grado[v.num_vertice]++;
            grado[e]++;
        }
    }
    int cImpares = 0;

    for (auto d : grado) {
        if (d % 2 == 1) {
            cImpares++;
        }
    }
    return (cImpares == 0 || cImpares == 2);
}

int Costo[100][100], n;

int getMin(int distancia[], bool visitado[]) {
    int clave = 0;
    int min = INT_MAX;
    for (int i = 0; i < n; i++) {
        if (!visitado[i] && distancia[i] < min) {
            min = distancia[i];
            clave = i;
        }
    }
    return clave;
}

void imprime(int distancia[], int par[]) {
    for (int i = 0; i < n; i++) {
        int temp = par[i];
        cout << i << " <- ";
        while (temp != -1)
        {
            cout << temp << " <- ";
            temp = par[temp];
        }
        cout << endl;
        cout << "::::Distancia = " << distancia[i];
        cout << endl;
    }
}


void dijkstra(int fuente) {
    int par[100], distancia[100];
    bool visitado[100] = { 0 };
    fill(distancia, distancia + n, INT_MAX);

    distancia[fuente] = 0;
    par[fuente] = -1;

    for (int g = 0; g < n - 1; g++) {
        int u = getMin(distancia, visitado);
        visitado[u] = true;
        cout << " min = " << u << endl;
        for (int v = 0; v < n; v++) {
            if (!visitado[v] && (distancia[u] + Costo[u][v]) < distancia[v] && Costo[u][v] != 9999)
            {
                par[v] = u;
                distancia[v] = distancia[u] + Costo[u][v];
            }
        }
    }

    imprime(distancia, par);
}

int main() {
    Grafo g({ Vertice(0, {1, 3}), Vertice(1, {0, 2, 4, 5}), Vertice(2, {1, 3}), Vertice(3, {0, 2}), Vertice(4, {1, 3, 5, 6}),
        Vertice(5, {1, 4, 6, 7}), Vertice(6, {4, 5, 7, 8}), Vertice(7, {5, 6, 8}), Vertice(8, {6, 7}) });
    std::cout << g.EsCaminable() << std::endl;
    cout << "Ingresar n : ";
    cin >> n;
    cout << "Ingresar dinamica de red espaciotemporal : \n";
    //esto es lo que gu�a las decisiones sobre el destino de las c�lulas
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++)cin >> Costo[i][j];
    }
    int src;
    cout << "\nIngresar fuente : ";  cin >> src;
    dijkstra(src);
}